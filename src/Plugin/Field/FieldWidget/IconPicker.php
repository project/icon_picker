<?php

declare(strict_types=1);

namespace Drupal\icon_picker\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'icon_picker' widget.
 *
 * @FieldWidget(
 *   id = "icon_picker",
 *   label = @Translation("Icon Picker"),
 *   field_types = {
 *     "text",
 *     "string",
 *   }
 * )
 */
class IconPicker extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element['value'] = $element + [
      '#type' => 'textfield',
      '#default_value' => $items[$delta]->value ?? NULL,
      '#size' => 30,
      '#prefix' => '<div class="icon-picker-wrapper"><span class="icon-picker-preview"></span>',
      '#suffix' => '</div>',
      '#attributes' => [
        'class' => [
          'IconPickerVanillaIconPicker',
        ],
      ],
      '#attached' => [
        'library' => [
          'icon_picker/vanilla-icon-picker',
          'icon_picker/vanilla-icon-picker-theme-bootstrap',
        ],
      ],
    ];
    return $element;
  }

}
