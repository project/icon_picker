(function (Drupal, once) {
  Drupal.behaviors.IconPickerVanilla = {
    attach(context) {
      const elements = once('IconPickerVanilla', '.IconPickerVanillaIconPicker', context);
      elements.forEach(function (element) {
        const iconPreview = element.parentNode.previousElementSibling;
        element.onfocus = function () {
          const iconPickerInput = new IconPicker(element, {
            theme: 'bootstrap-5',
            iconSource: [
              {
                key: 'ic',
                prefix: 'ic:',
                url: 'https://raw.githubusercontent.com/iconify/icon-sets/master/json/ic.json'
              },
              {
                key: 'mdi',
                prefix: 'mdi:',
                url: 'https://raw.githubusercontent.com/iconify/icon-sets/master/json/mdi.json'
              }
            ],
            closeOnSelect: true
          });

          iconPickerInput.on('select', (icon) => {
            if (iconPreview.innerHTML !== '') {
              iconPreview.innerHTML = '';
            }
            iconPreview.innerHTML = icon.svg;
          });
        };

        const inputValue = element.value;
        if (inputValue) {
          iconPreview.innerHTML = '<iconify-icon style="font-size: 35px" icon="' + inputValue + '"></iconify-icon>';
        }
      });
    }
  };
}(Drupal, once));
